#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Asociacio\u0144 la Jaquer\xeda'
SITENAME = u'Asociaci\xf3n La Jaquer\xeda'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Madrid'

DEFAULT_LANG = u'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Hacklab Almería', 'https://hacklabalmeria.net/'),
         )

# Social widget
SOCIAL = (('Foro en el Hacklab Almería', 'https://foro.hacklabalmeria.net/c/la-jaqueria'),
          ('Linkedin', 'https://es.linkedin.com/company/lajaqueria'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Gitlab necesita que la salida se genere en el directorio «public»
OUTPUT_PATH = 'public/'
