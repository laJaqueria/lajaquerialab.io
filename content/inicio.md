Title: Asociación la Jaquería
Date: 2018-08-08 13:40
Category: General

La Jaquería es una asociación sin ánimo de lucro que busca, entre otros fines, la consecución de un espacio físico desde el que «promover la experimentación y el conocimiento tecnológico, científico, y artístico a través de la colaboración y educación social», es decir, constituir un hackspace en la ciudad de Almería.

